package webclient;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@RestController
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class MainController {

    private final WebClient webClient;
    @Value("${url}")
    private String url;

    @Autowired
    public MainController(WebClient.Builder webClient) {
        this.webClient = webClient.build();
    }

    @GetMapping("/")
    public Mono<String> getPostsByPage(@RequestParam Integer page) {
        return webClient
                .get()
                .uri(url, page)
                .retrieve().bodyToMono(String.class);
    }

    @GetMapping("/user/{characterId}")
    public Mono<String> getPostById(@PathVariable Integer characterId) {
        return webClient
                .get()
                .uri(url + "/{characterId}", characterId)
                .retrieve().bodyToMono(String.class);
    }
}
