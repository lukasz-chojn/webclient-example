package webclient

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.web.reactive.server.WebTestClient
import spock.lang.Specification

@SpringBootTest
class MainControllerTest extends Specification {

    @Autowired
    private WebTestClient webTestClient

    def setup() {
        webTestClient = WebTestClient
                .bindToServer()
                .baseUrl("http://localhost:8080")
                .build()
    }

    def "Method should return posts with page"() {

        expect: "should return proper message"
        webTestClient
                .get()
                .uri(builder -> builder
                        .path("/")
                        .queryParam("page", 2)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
    }

    def "Method should return posts with specific id"() {

        expect: "should return proper message"
        webTestClient
                .get()
                .uri("/user/{characterId}", 1775)
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
    }
}
