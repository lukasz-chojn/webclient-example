package webclient


import org.springframework.boot.test.context.SpringBootTest
import org.springframework.web.reactive.function.client.WebClient
import spock.lang.Specification

@SpringBootTest
class WebclientExampleApplicationTest extends Specification {

    MainController mainController = new MainController(Mock(WebClient.Builder))

    def "Application context should start with autowired bean"() {

        expect: "injecting controller bean"
        mainController
    }
}
